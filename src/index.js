import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './css/App.css';

import React from 'react';

import { render } from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import Root from './components/Root';
import reducers from './reducers';
import registerServiceWorker from './registerServiceWorker';
import { getAllProducts } from './actions'

import { browserHistory as history } from 'react-router';

let store = createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk)
);

store.dispatch(getAllProducts())
window.store = store; // debugging

render(
  <Root store={store} history={history} />,
  document.getElementById('root')
)

registerServiceWorker();
