import client from './api/Client'

// action types
export const FETCH_PRODUCTS = 'FETCH_PRODUCTS'
export const SET_TYPE_FILTER = 'SET_TYPE_FILTER'
export const ADD_TO_BASKET = 'ADD_TO_BASKET'
export const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET'

// other constants
export const TypeFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_FRUITS: 'SHOW_FRUITS',
  SHOW_VEGETABLES: 'SHOW_VEGETABLES'
}
export const TypeFilterTitles = {
  SHOW_ALL: 'All',
  SHOW_FRUITS: 'Only fruits',
  SHOW_VEGETABLES: 'Only vegetables'
}

// action creators
function fetchProducts(products) {
  return { type: FETCH_PRODUCTS, products }
}

export function addToBasket(id) {
  return { type: ADD_TO_BASKET, id }
}

export function removeFromBasket(id) {
  return { type: REMOVE_FROM_BASKET, id }
}

export function setTypeFilter(filter) {
  return { type: SET_TYPE_FILTER, filter }
}


// dispatchers
export const getAllProducts = () => dispatch => {
  client.getProducts(products => {
    dispatch(fetchProducts(products))
  })
}
