import React from 'react'
import VisibleProductList from '../containers/VisibleProductList'
import HomeHeader from '../containers/HomeHeader'
import FilterBox from './FilterBox'

const App = () => {
  return (
    <div className='container' >
      <div className='panel panel-primary'>
        <HomeHeader />
        <FilterBox />
        <VisibleProductList />
      </div>
    </div>
  )
}

export default App
