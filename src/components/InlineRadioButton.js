import React from 'react'
import PropTypes from 'prop-types';

const InlineRadioButton = ({ active, title, onChange }) => (
  <label className='radio-inline'>
    <input
      type='radio'
      onChange={onChange}
      checked={active}
    /> { title }
  </label>
)

InlineRadioButton.propTypes = {
  active: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

export default InlineRadioButton
