import React from 'react'
import PropTypes from 'prop-types';

const BasketItem = ({ name, price, quantity, subTotal, onAddCb, onRemoveCb }) => {
  return (
    <div className='row'>
      <div className='col-md-4'>
        {name}
      </div>
      <div className='col-md-2'>
        Price:
        ${ price }
      </div>
      <div className='col-md-4 text-center'>
        <i className='glyphicon glyphicon-plus' onClick={onAddCb}></i>
        <span className='badge'>{ quantity }</span>
        <i className='glyphicon glyphicon-minus' onClick={onRemoveCb}></i>
      </div>
      <div className='col-md-2 text-right'>
        Sub total:
        ${ subTotal }
      </div>
    </div>
  )
}

BasketItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  subTotal: PropTypes.number.isRequired,
  onAddCb: PropTypes.func.isRequired,
  quantity: PropTypes.number.isRequired,
  onRemoveCb: PropTypes.func.isRequired
}

export default BasketItem
