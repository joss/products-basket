import React from 'react'
import PropTypes from 'prop-types';
import Product from './Product'

const ProductList = ({ products, onAddToBasket }) => (
  <table className='table table-bordered table-hover'>
    <thead>
      <tr>
        <th>Name Of Product</th>
        <th>Kind Of</th>
        <th>Price, $</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      {products.map(product =>
        <Product
          key={product.id}
          {...product}
          onAddCb={ () => onAddToBasket(product.id) }
        />
      )}
    </tbody>
  </table>
)

ProductList.propTypes = {
  onAddToBasket: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.oneOf(['fruit', 'vegetable']).isRequired,
    price: PropTypes.number.isRequired
  }).isRequired).isRequired
}

export default ProductList
