import React from 'react'

import { Link } from 'react-router'

const BasketHeader = () => {
  return (
    <div className='panel-heading'>
      <div className='row'>
        <div className='col-md-1'>
          <Link to='/' className='glyphicon glyphicon-home' />
        </div>
        <div className='col-md-11'>
          Products ready for checkout
        </div>
      </div>
    </div>
  )
}

export default BasketHeader
