import React from 'react'
import PropTypes from 'prop-types';

const Product = ({ name, type, price, onAddCb }) => (
  <tr>
    <td>{ name }</td>
    <td>{ type }</td>
    <td>{ price }</td>
    <td className='text-center'>
      <i className='glyphicon glyphicon-plus' onClick={onAddCb}></i>
    </td>
  </tr>
)

Product.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['fruit', 'vegetable']).isRequired,
  price: PropTypes.number.isRequired,
  onAddCb: PropTypes.func.isRequired
}

export default Product
