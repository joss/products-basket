import React from 'react'
import BasketHeader from '../components/BasketHeader'
import BasketTotal from '../containers/BasketTotal'
import BasketList from '../containers/BasketList'

const Basket = () => {
  return (
    <div className='container' >
      <div className='panel panel-primary'>
        <BasketHeader />
        <BasketTotal />
        <BasketList />
      </div>
    </div>
  )
}

export default Basket
