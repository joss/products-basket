import React from 'react'
import { TypeFilters } from '../actions'
import TypeFilterButton from '../containers/TypeFilterButton'

const FilterBox = () => (
  <div className='panel-body'>
    {Object.keys(TypeFilters).map(key =>
      <TypeFilterButton key={key} filter={key}/>
    )}
  </div>
)

export default FilterBox
