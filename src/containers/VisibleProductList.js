import { connect } from 'react-redux'
import { addToBasket } from '../actions'
import ProductList from '../components/ProductList'

const applyTypeFilter = (products, filter) => {
  switch (filter) {
    case 'SHOW_ALL':
      return products
    case 'SHOW_FRUITS':
      return products.filter(p => p.type === 'fruit')
    case 'SHOW_VEGETABLES':
      return products.filter(p => p.type === 'vegetable')
    default:
      return products
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    products: applyTypeFilter(state.products, state.typeFilter)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToBasket: (id) => {
      dispatch(addToBasket(id))
    }
  }
}

const VisibleProductList = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList)

export default VisibleProductList
