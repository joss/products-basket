import { connect } from 'react-redux'
import { setTypeFilter, TypeFilterTitles } from '../actions'
import InlineRadioButton from '../components/InlineRadioButton'

const mapStateToProps = (state, ownProps) => {
  return {
    active: ownProps.filter === state.typeFilter
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    title: TypeFilterTitles[ownProps.filter],
    onChange: () => {
      dispatch(setTypeFilter(ownProps.filter))
    }
  }
}

const TypeFilterButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(InlineRadioButton)

export default TypeFilterButton
