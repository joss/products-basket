import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { addToBasket, removeFromBasket } from '../actions'

import BasketItem from '../components/BasketItem'

function collectProductData (products, basketIds) {
  return Object.entries(basketIds).reduce(function (memo, [id, quantity]) {
    let product = products.find(p => { return p.id === parseInt(id, 10) })
    let productInfo = {...product, quantity, subTotal: product.price * quantity}

    memo.push(productInfo)

    return memo
  }, [])
}

const mapStateToProps = (state, ownProps) => {
  return {
    chosenProducts: collectProductData(state.products, state.basketIds)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddToBasket: (id) => {
      dispatch(addToBasket(id))
    },
    onRemoveFromBasket: (id) => {
      dispatch(removeFromBasket(id))
    },
  }
}

const BasketList = ({ chosenProducts, onAddToBasket, onRemoveFromBasket }) => {
  return (
    <div className='pad-20'>
      {chosenProducts.map(product =>
        <BasketItem
          key={product.id}
          {...product}
          onAddCb={ () => onAddToBasket(product.id) }
          onRemoveCb={ () => onRemoveFromBasket(product.id) }
        />
      )}
    </div>
  )
}

BasketList.propTypes = {
  chosenProducts:  PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired,
    subTotal: PropTypes.number.isRequired,
  }).isRequired).isRequired,
  onAddToBasket: PropTypes.func.isRequired,
  onRemoveFromBasket: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(BasketList)
