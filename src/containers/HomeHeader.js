import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

import { Link } from 'react-router'

function calculateTotalPrice(products, basket) {
  let sum = 0

  for (let [id, quantity] of Object.entries(basket)) {
    let product = products.find(p => { return p.id === parseInt(id, 10) })
    if (product) {
      sum += product.price * quantity
    }
  }

  return sum;
}

const mapStateToProps = (state, ownProps) => {
  return {
    itemsCount: Object.keys(state.basketIds).length,
    totalPrice: calculateTotalPrice(state.products, state.basketIds)
  }
}

const HomeHeader = ({ itemsCount, totalPrice }) => {
  return (
    <div className='panel-heading'>
      <div className='row'>
        <div className='col-md-4'>
          Products available
        </div>
        <div className='col-md-4'>
          Total price:
          ${ totalPrice }
        </div>
        <div className='col-md-4 text-right'>
          <span className='badge'>{ itemsCount }</span>
          <Link to='/basket' className='glyphicon glyphicon-shopping-cart' />
        </div>
      </div>
    </div>
  )
}

HomeHeader.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  totalPrice: PropTypes.number.isRequired
}

export default connect(mapStateToProps)(HomeHeader)
