import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux'

//TODO: extract method
function calculateTotalPrice(products, basket) {
  let sum = 0

  for (let [id, quantity] of Object.entries(basket)) {
    let product = products.find(p => { return p.id === parseInt(id, 10) })
    if (product) {
      sum += product.price * quantity
    }
  }

  return sum;
}

const mapStateToProps = (state, ownProps) => {
  return {
    totalPrice: calculateTotalPrice(state.products, state.basketIds)
  }
}

const BasketTotal = ({ totalPrice }) => {
  return (
    <div className='panel-body text-right'>
      Total: ${totalPrice}
    </div>
  )
}

BasketTotal.propTypes = {
  totalPrice: PropTypes.number.isRequired
}

export default connect(mapStateToProps)(BasketTotal)
