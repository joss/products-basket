import { combineReducers } from 'redux'
import { FETCH_PRODUCTS, SET_TYPE_FILTER, ADD_TO_BASKET, REMOVE_FROM_BASKET, TypeFilters } from './actions'
const { SHOW_ALL } = TypeFilters

function typeFilter(state = SHOW_ALL, action) {
  switch (action.type) {
    case SET_TYPE_FILTER:
      return action.filter
    default:
      return state
  }
}

function products(state = [], action) {
  switch (action.type) {
    case FETCH_PRODUCTS:
      return [
        ...state,
        ...action.products
      ]
    default:
      return state
  }
}

function basketIds(state = {}, action) {
  let result = {...state}

  switch (action.type) {
    case ADD_TO_BASKET:
      result[action.id] = (result[action.id] || 0) + 1
      return result
    case REMOVE_FROM_BASKET:
      let initialValue = result[action.id] || 0

      if (initialValue > 0) {
        result[action.id] = initialValue - 1
      }
      if (result[action.id] === 0) {
        delete result[action.id]
      }

      return result
    default:
      return state
  }
}

const reducers = combineReducers({
  typeFilter,
  products,
  basketIds
})

export default reducers
